{{ range ls "domains" }}{{ .Value }}:
  - whitelist:
    {{ range ls "whitelist/app01-vm-prd.mt.local" }}- permit {{ .Value }};
    {{end}}
{{end}}

----

{{ range $key, $server := tree "domains/" | explode }}
{{ $name := $server }} {{ $server }}
  - whitelist:
	{{ range $key, $pairs := tree ($name | printf "service/%s/") | explode }}
        {{ $name}}: {{ $pairs }}
    {{ end }}
{{ end }}
