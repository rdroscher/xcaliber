# The logs for each virtual hosts should be stored as follows;
# /var/log/nginx/$vhost_name_access.log and _error.log
# The names of the domains app01-vm-prd.mt.local and app02-vm-prd.mt.local need to be passed either from a script, during build or during provisioning – as long as these can be changed before running the build.

# setup hosts file to resolve app01 & app02
cat > /tmp/hosts << 'endmsg'
127.0.0.1	localhost app01-vm-prd.mt.local app02-vm-prd.mt.local
endmsg
sudo mv /tmp/hosts /etc/hosts

# create web roots
sudo mkdir -p /var/www/app01
sudo mkdir -p /var/www/app02

# dummy index pages for each domain
cat > /tmp/app1.html << 'endmsg'
app01 here!
endmsg
sudo mv /tmp/app1.html /var/www/app01/index.html

cat > /tmp/app2.html << 'endmsg'
app02 here
endmsg
sudo mv /tmp/app2.html /var/www/app02/index.html

# install nginx
sudo yum -y install nginx
sudo chkconfig nginx on

# nginx config 1
cat > /tmp/app01.conf <<'endmsg'
server {
    server_name app01-vm-prd.mt.local;
    access_log /var/log/nginx/app01-vm-prd_access.log main;
    error_log /var/log/nginx/app01-vm-prd_error.log info;

    root /var/www/app01;
  }
endmsg

sudo mv /tmp/app01.conf /etc/nginx/conf.d/app01.conf

# nginx config 2
cat > /tmp/app02.conf <<'endmsg'
server {
    server_name app02-vm-prd.mt.local;
    access_log /var/log/nginx/app02-vm-prd_access.log main;
    error_log /var/log/nginx/app02-vm-prd_error.log info;

    root /var/www/app02;
  }
endmsg

sudo mv /tmp/app02.conf /etc/nginx/conf.d/app02.conf

# start nginx
sudo systemctl start nginx

