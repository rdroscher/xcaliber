# README #

This is the summary of the test and it was a real learning experience with a lot of new technologies I've not used before. 

It's important to mention I had to research and learn the following from scratch:

* consul (I'd normally use AWS cloudformation and ECS to deploy revisioned containers from ECR)
* vagrant to deploy vms in virtualbox (I'd normally use AWS ECS and docker)
* consol-template (I'd keep config in revisioned buckets which are moved onto prod with blue/green deployments)
* go templating language
* packer (I'd use bamboo to build docker images then push to AWS ECR)

Prerequisites I had to install:

* packer 1.5.5
* virtualbox 6.1
* consul 1.7.2
* consul-template 0.24.1
* vagrant 2.2.7

### Question 1 - Create a Consul cluster with 2 nodes

For this I used [Vagrantfile](https://bitbucket.org/rdroscher/xcaliber/src/master/question1/Vagrantfile) to deploy virtualbox linux vms (agent-one declared server, agent-two declared client)

```
vagrant up
vagrant ssh n1
vagrant ssh n2
```

Booting each node

```consul agent -config-dir=/etc/consul.d```

From Node1 join Node2

```consul join 172.20.20.10 172.20.20.11```

![Two nodes connected](/images/two-nodes-connected.png)


### Question 1 - Create Keys in KV store & run consul-template to generate deliverable ###

```
consul kv put domains/domain1 "app01-vm-prd.mt.local"
consul kv put domains/domain2 "app02-vm-prd.mt.local"
consul kv put whitelist/app01-vm-prd.mt.local/whitelist-1 10.0.0.8
consul kv put whitelist/app01-vm-prd.mt.local/whitelist-2 10.1.8.3
consul kv put whitelist/app01-vm-prd.mt.local/whitelist-3 10.5.2.4
consul kv put whitelist/app02-vm-prd.mt.local/whitelist-1 192.168.20.9
consul kv put whitelist/app02-vm-prd.mt.local/whitelist-2 192.168.50.7
consul kv put whitelist/app02-vm-prd.mt.local/whitelist-3 192.168.20.5

consul-template -template="domains.tpl:domains" -once
consul-template -template="whitelist.tpl:whitelist" -once
```

**Deliverables:**

* Consul template file for domains - [domains.tpl](https://bitbucket.org/rdroscher/xcaliber/src/master/question1/domains.tpl)
* Consul template file for whitelist - [whitelist.tpl](https://bitbucket.org/rdroscher/xcaliber/src/master/question1/whitelist.tpl), INCOMPLETE - First start I hardcode the hosts it looks okay but obviously doesn't get second host values. Second attempt I tried to loop domains folder and attempt to use value to recurse the whitelist subfolders. I feel I'm on the right lines with some examples I found but I ran out of time and this is how far I've got. 
* Consul configs - [node1](https://bitbucket.org/rdroscher/xcaliber/src/master/question1/n1.hcl).. and [node2](https://bitbucket.org/rdroscher/xcaliber/src/master/question1/n2.hcl)

### Question 2 - 	Use any of the tools (Packer, Bash, Salt, Ansible) to build a VirtualBox image which contains configuration of Nginx running with 2 virtual hosts. 

**Attempt 1 -** I customised a packer template to build an ubuntu 16.04 image from releases.ubuntu.org which didn't work because the http server is returning 404 for the pressed.cfg file so the install doesn't work. I've spent a good few hours trying to figure it out with no success, http is running on the port advertised but I can't find a way to expose the cfg content. I then hosted the cfg file on an ec2 instance running nginx over http but the virtualbox-iso can't see that either as it doesn't appear to have internet connectivity.

```
packer validate ubuntu-template.json
Template validated successfully.
packer build ubuntu-template.json
```

After many attempts to iron out the bugs I'm now getting a kernel panic from virtual box on boot.

![Kernel Panic](/images/kernel-panic.png)

**Attempt 2 -** Time to try another distro, centos 7 and it's looking much better

[centos-7.7-x86_64.json](https://bitbucket.org/rdroscher/xcaliber/src/master/question2/centos/centos-7.7-x86_64.json) is the virtualbox-iso config template

[nginx.sh](https://bitbucket.org/rdroscher/xcaliber/src/master/question2/centos/scripts/nginx.sh) is the configuration that while not super elegant:

* installs nginx
* sets to load on boot
* cat's in the two nginx server blocks into files located /etc/nginx/conf.d/app01.conf & /etc/nginx/conf.d/app02.conf respectively
* configures nginx for app02-vm-prd_access.log & app02-vm-prd_error.log (and likewise for app01)

You can confirm it's correct by running packer to build the image

```
packer validate centos-7.7-x86_64.json
Template validated successfully.
packer build centos-7.7-x86_64.json
```

Then prove config from [nginx.sh](https://bitbucket.org/rdroscher/xcaliber/src/master/question2/centos/scripts/nginx.sh) goes into virtual machine image, populates nginx config and returns sample page(s)

```
curl http://app01-vm-prd.mt.local
curl http://app02-vm-prd.mt.local
```


